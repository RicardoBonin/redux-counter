import React from 'react'
import { configure, shallow } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import { DisplayCounter } from './DisplayCounter'

configure({ adapter: new Adapter() })

describe('<DisplayCounter />', ()=>{
    it('mounts', ()=>{
        const wrapper = shallow(<DisplayCounter count={10} />)
        expect(wrapper.contains(<p>O contador está em 10</p>))
            .toBe(true)
    })
})
