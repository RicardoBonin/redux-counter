import React from 'react'
import Counter from './Counter'
import counterReducer from './reducer'
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import DisplayCounter from './DisplayCounter'
import Ola from './Ola'

let store = createStore(counterReducer) 

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <Counter />
        <DisplayCounter />
        <Ola nome='Ricardo' />
      </div>
    </Provider>
  )
}

export default App
