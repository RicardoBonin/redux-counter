import React from 'react'
import PropTypes from 'prop-types'

const Ola = (props) => {
    return(
        <span>Ola.{props.nome}</span>
    )
}
Ola.propTypes = {
    nome: PropTypes.string
}
Ola.defaultProps = {
    nome: 'Zé Ninguém'
}

export default Ola